# remove-provision/README.md

Will remove all users from the `provision_users` group. 

No default is set, since this is destructive. 