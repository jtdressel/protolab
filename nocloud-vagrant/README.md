# `nocloud-vagrant/README.md`

See ../provision.md for context. 

This vagrant enviornment is useful for making a cloud-init datastore / `seed file`


```
vagrant up
vagrant ssh
cd /vagrant
cloud-localds --hostname zamira zamira_ssh_dhcp_pw.img user_data_userpass
cloud-localds --network-config network.cfg --hostname frodo frodo.iso user_data_userpass
```

`user_data_userpass` contains setup with my usual user accounts/ssh keys. It also contains a `provision-user` with a password of `userpass`. This user should be removed. (I use it to set up static ip)

To bake-in a new password, use `mkpasswd --method=SHA-512 --rounds=4096` to generate. 
