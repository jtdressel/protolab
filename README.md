On synology I installed Ubuntu 18.04 LTS. 

provision-user@provision

Should be able to clone the vm, run playbooks, and change the hostname. 


## lUbuntu 18.04 base
Snapshot 02: qemu-guest-agent 
Snapshot 01: has ssh server installed

## xenial-srv-base - Ubuntu 16.04

Does not have the desktop environment installed. Created to support the Unifi controller. 

### Snapshot 1:

* ubuntu 16.04 installed
* ssh server installed
* provisioning key copied


### Snapshot 1:

* ubuntu 16.04 installed
* ssh server installed
* provisioning key copied

## TODO:

* Rekey ssh role
* sensu / external alerting
* Centrailized log management
* Reconsider .ssh/config
* Automate pihole
* Get pi-hole doing DHCP
* (in ansible) add users to journalctl group

### unifi:

* Provide TLS cert
* Provide static IP
* Decide if it can be a bionic box
* Move galaxy requirements into a role?

## Provision on ESXI


* Import virtual appliance
* thin provision
* deselect `Power on automatically`
* While the file is uploading, create the seed image. See `nocloud-vagrant/`
* Edit vm - set seed file as CD 1
* Make sure cd is connected
* Boot
* Use conosle and log in as provision user. Print out ip address
* (You could also do this all via console, but iTerm is more enjoyable)
* ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null DHCP_IP
* Disable cloudinit networking `echo "network: {config: disabled}" | sudo tee -a /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg`
* remove generated netplan `sudo rm /etc/netplan/50-cloud-init.yaml`
* Add something like the below to
`sudo vim /etc/netplan/5-config.yaml`

```
network:
    ethernets:
        ens192:
            dhcp4: false
            addresses:
                    - 10.8.2.14/16
            gateway4: 10.8.1.1
            nameservers:
                    addresses: [8.8.8.8]
    version: 2

```

* Add to ~/.ssh/config
* Add to ansible inventory
* Run baseline (should remove provision-user)